<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaEditoras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liv_editoras', function (Blueprint $table) {
            $table->increments('id_editora');
            $table->string('editora');            
            $table->unsignedBiginteger('id_user'); //em users é bigint
            $table->foreign('id_user')->references('id')->on('users');
            $table->timestamp('dt_cadastro')->nullable();
            $table->timestamp('dt_alteracao')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
