<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaAutores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liv_autores', function (Blueprint $table) {
            $table->Increments('id_autor');
            $table->unsignedBiginteger('id_user'); //em users é bigint
            $table->foreign('id_user')->references('id')->on('users');
            $table->string('autor');                        
            $table->timestamp('dt_cadastro')->nullable();
            $table->timestamp('dt_alteracao')->nullable();            
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
