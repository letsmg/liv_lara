<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaLivros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liv_livros', function (Blueprint $table) {
            $table->increments('id_livro');
            $table->unsignedInteger('id_autor');
            $table->unsignedInteger('id_editora');
            $table->unsignedBiginteger('id_user'); //em users é bigint
            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_autor')->references('id_autor')->on('liv_autores');
            $table->foreign('id_editora')->references('id_editora')->on('liv_editoras');            
            $table->string('titulo');
            $table->string('capa');
            $table->string('valor');
            $table->string('f1',50)->nullable();
            $table->string('f2',50)->nullable();
            $table->string('f3',50)->nullable();
            $table->timestamp('dt_cadastro')->nullable();
            $table->timestamp('dt_alteracao')->nullable();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
