-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 25-Jun-2019 às 15:23
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog_ci`
--
CREATE DATABASE IF NOT EXISTS `blog_ci` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `blog_ci`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `artigos`
--
-- Error reading structure for table blog_ci.artigos: #1932 - Table 'blog_ci.artigos' doesn't exist in engine

-- --------------------------------------------------------

--
-- Estrutura da tabela `preferencias`
--
-- Error reading structure for table blog_ci.preferencias: #1932 - Table 'blog_ci.preferencias' doesn't exist in engine

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--
-- Error reading structure for table blog_ci.usuarios: #1932 - Table 'blog_ci.usuarios' doesn't exist in engine
--
-- Database: `blog_lara`
--
CREATE DATABASE IF NOT EXISTS `blog_lara` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `blog_lara`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--
-- Error reading structure for table blog_lara.categorias: #1932 - Table 'blog_lara.categorias' doesn't exist in engine

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--
-- Error reading structure for table blog_lara.migrations: #1932 - Table 'blog_lara.migrations' doesn't exist in engine

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--
-- Error reading structure for table blog_lara.password_resets: #1932 - Table 'blog_lara.password_resets' doesn't exist in engine

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--
-- Error reading structure for table blog_lara.users: #1932 - Table 'blog_lara.users' doesn't exist in engine
--
-- Database: `laravel_dock`
--
CREATE DATABASE IF NOT EXISTS `laravel_dock` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `laravel_dock`;
--
-- Database: `livraria`
--
CREATE DATABASE IF NOT EXISTS `livraria` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `livraria`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `liv_autores`
--

CREATE TABLE `liv_autores` (
  `id_autor` int(11) NOT NULL,
  `autor` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `liv_editoras`
--

CREATE TABLE `liv_editoras` (
  `id_editora` int(11) NOT NULL,
  `editora` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `liv_livros`
--

CREATE TABLE `liv_livros` (
  `id_livro` int(11) NOT NULL,
  `id_editora` int(11) NOT NULL,
  `id_autor` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `valor` float(3,2) NOT NULL,
  `capa` varchar(50) NOT NULL,
  `f1` varchar(50) DEFAULT NULL,
  `f2` varchar(50) DEFAULT NULL,
  `f3` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `liv_usuarios`
--

CREATE TABLE `liv_usuarios` (
  `id_usu` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `senha` varchar(62) NOT NULL,
  `nivel_acesso` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 para padrão, 1 admin, 2 técnico',
  `avatar` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `liv_autores`
--
ALTER TABLE `liv_autores`
  ADD PRIMARY KEY (`id_autor`);

--
-- Indexes for table `liv_editoras`
--
ALTER TABLE `liv_editoras`
  ADD PRIMARY KEY (`id_editora`);

--
-- Indexes for table `liv_livros`
--
ALTER TABLE `liv_livros`
  ADD PRIMARY KEY (`id_livro`),
  ADD KEY `fkey_smrgicupap` (`id_editora`),
  ADD KEY `fkey_iikgmedlsr` (`id_autor`);

--
-- Indexes for table `liv_usuarios`
--
ALTER TABLE `liv_usuarios`
  ADD PRIMARY KEY (`id_usu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `liv_autores`
--
ALTER TABLE `liv_autores`
  MODIFY `id_autor` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `liv_editoras`
--
ALTER TABLE `liv_editoras`
  MODIFY `id_editora` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `liv_livros`
--
ALTER TABLE `liv_livros`
  MODIFY `id_livro` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `liv_usuarios`
--
ALTER TABLE `liv_usuarios`
  MODIFY `id_usu` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `liv_livros`
--
ALTER TABLE `liv_livros`
  ADD CONSTRAINT `fkey_iikgmedlsr` FOREIGN KEY (`id_autor`) REFERENCES `liv_autores` (`id_autor`),
  ADD CONSTRAINT `fkey_smrgicupap` FOREIGN KEY (`id_editora`) REFERENCES `liv_editoras` (`id_editora`);
--
-- Database: `liv_lara`
--
CREATE DATABASE IF NOT EXISTS `liv_lara` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `liv_lara`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assunto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mensagem` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `liv_autores`
--

CREATE TABLE `liv_autores` (
  `id_autor` int(10) UNSIGNED NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `autor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dt_cadastro` timestamp NULL DEFAULT NULL,
  `dt_alteracao` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `liv_editoras`
--

CREATE TABLE `liv_editoras` (
  `id_editora` int(10) UNSIGNED NOT NULL,
  `editora` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `dt_cadastro` timestamp NULL DEFAULT NULL,
  `dt_alteracao` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `liv_livros`
--

CREATE TABLE `liv_livros` (
  `id_livro` int(10) UNSIGNED NOT NULL,
  `id_autor` int(10) UNSIGNED NOT NULL,
  `id_editora` int(10) UNSIGNED NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `capa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `f1` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `f2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `f3` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dt_cadastro` timestamp NULL DEFAULT NULL,
  `dt_alteracao` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `liv_autores`
--
ALTER TABLE `liv_autores`
  ADD PRIMARY KEY (`id_autor`),
  ADD KEY `liv_autores_id_user_foreign` (`id_user`);

--
-- Indexes for table `liv_editoras`
--
ALTER TABLE `liv_editoras`
  ADD PRIMARY KEY (`id_editora`),
  ADD KEY `liv_editoras_id_user_foreign` (`id_user`);

--
-- Indexes for table `liv_livros`
--
ALTER TABLE `liv_livros`
  ADD PRIMARY KEY (`id_livro`),
  ADD KEY `liv_livros_id_user_foreign` (`id_user`),
  ADD KEY `liv_livros_id_autor_foreign` (`id_autor`),
  ADD KEY `liv_livros_id_editora_foreign` (`id_editora`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `liv_autores`
--
ALTER TABLE `liv_autores`
  MODIFY `id_autor` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `liv_editoras`
--
ALTER TABLE `liv_editoras`
  MODIFY `id_editora` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `liv_livros`
--
ALTER TABLE `liv_livros`
  MODIFY `id_livro` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `liv_autores`
--
ALTER TABLE `liv_autores`
  ADD CONSTRAINT `liv_autores_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `liv_editoras`
--
ALTER TABLE `liv_editoras`
  ADD CONSTRAINT `liv_editoras_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `liv_livros`
--
ALTER TABLE `liv_livros`
  ADD CONSTRAINT `liv_livros_id_autor_foreign` FOREIGN KEY (`id_autor`) REFERENCES `liv_autores` (`id_autor`),
  ADD CONSTRAINT `liv_livros_id_editora_foreign` FOREIGN KEY (`id_editora`) REFERENCES `liv_editoras` (`id_editora`),
  ADD CONSTRAINT `liv_livros_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);
--
-- Database: `manualdoesperto`
--
CREATE DATABASE IF NOT EXISTS `manualdoesperto` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `manualdoesperto`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `xy_anuncios`
--

CREATE TABLE `xy_anuncios` (
  `id_anuncio` int(4) NOT NULL,
  `id_topico` int(11) NOT NULL,
  `id_topico2` int(11) NOT NULL,
  `titulo_anuncio` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `url_anuncio` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `plataforma` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `img_anuncio` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `dt_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ativo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `xy_contato`
--

CREATE TABLE `xy_contato` (
  `id_contato` int(11) NOT NULL,
  `nome` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mensagem` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dt_mensagem` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ativo` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `xy_grupos`
--

CREATE TABLE `xy_grupos` (
  `id_grupo` int(11) NOT NULL,
  `id_usu` int(11) NOT NULL,
  `nome_grupo` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dt_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `xy_posts`
--

CREATE TABLE `xy_posts` (
  `id_post` int(11) NOT NULL,
  `id_usu` int(11) NOT NULL,
  `id_topico` int(11) NOT NULL,
  `id_topico2` int(11) DEFAULT NULL,
  `titulo_post` varchar(70) CHARACTER SET utf8mb4 DEFAULT NULL,
  `tag_title` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag_description` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_post` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_alt` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img1` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `link` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secao` varchar(2500) CHARACTER SET utf8mb4 DEFAULT NULL,
  `subtitulo2` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `secao2` varchar(2500) CHARACTER SET utf8mb4 DEFAULT NULL,
  `img2` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `link2` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `texto_link2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video2` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitulo3` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `img3` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `link3` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `texto_link3` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secao3` varchar(2500) CHARACTER SET utf8mb4 DEFAULT NULL,
  `video3` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dt_publicacao` timestamp NULL DEFAULT NULL,
  `ativo` tinyint(4) DEFAULT '0',
  `tipo` tinyint(1) DEFAULT '0' COMMENT '0 padrao, 1 anuncios'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `xy_preferencias`
--

CREATE TABLE `xy_preferencias` (
  `id_preferencia` int(11) NOT NULL,
  `nome_blog` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `num_destaques` int(11) DEFAULT NULL,
  `cor_tema` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `xy_topicos`
--

CREATE TABLE `xy_topicos` (
  `id_topico` int(11) NOT NULL,
  `titulo_topico` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_topico` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dt_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ativo` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `xy_usuarios`
--

CREATE TABLE `xy_usuarios` (
  `id_usu` int(11) NOT NULL,
  `nome` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `nick` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `senha` varchar(60) CHARACTER SET utf8mb4 DEFAULT NULL,
  `recupera_senha` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `nivel` tinyint(1) DEFAULT '0' COMMENT '0 padrão, 1 admin, 2 HIERARCA'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `xy_anuncios`
--
ALTER TABLE `xy_anuncios`
  ADD PRIMARY KEY (`id_anuncio`);

--
-- Indexes for table `xy_contato`
--
ALTER TABLE `xy_contato`
  ADD PRIMARY KEY (`id_contato`);

--
-- Indexes for table `xy_grupos`
--
ALTER TABLE `xy_grupos`
  ADD PRIMARY KEY (`id_grupo`),
  ADD KEY `new_index_2` (`id_usu`);

--
-- Indexes for table `xy_posts`
--
ALTER TABLE `xy_posts`
  ADD PRIMARY KEY (`id_post`),
  ADD KEY `new_index_1` (`id_usu`),
  ADD KEY `new_index_2` (`id_topico`),
  ADD KEY `new_index_3` (`id_topico2`);

--
-- Indexes for table `xy_preferencias`
--
ALTER TABLE `xy_preferencias`
  ADD PRIMARY KEY (`id_preferencia`);

--
-- Indexes for table `xy_topicos`
--
ALTER TABLE `xy_topicos`
  ADD PRIMARY KEY (`id_topico`);

--
-- Indexes for table `xy_usuarios`
--
ALTER TABLE `xy_usuarios`
  ADD PRIMARY KEY (`id_usu`),
  ADD UNIQUE KEY `new_index_1` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `xy_anuncios`
--
ALTER TABLE `xy_anuncios`
  MODIFY `id_anuncio` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xy_contato`
--
ALTER TABLE `xy_contato`
  MODIFY `id_contato` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xy_posts`
--
ALTER TABLE `xy_posts`
  MODIFY `id_post` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xy_preferencias`
--
ALTER TABLE `xy_preferencias`
  MODIFY `id_preferencia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xy_topicos`
--
ALTER TABLE `xy_topicos`
  MODIFY `id_topico` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `xy_usuarios`
--
ALTER TABLE `xy_usuarios`
  MODIFY `id_usu` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `xy_grupos`
--
ALTER TABLE `xy_grupos`
  ADD CONSTRAINT `fkey_ljgyzhwwaj` FOREIGN KEY (`id_usu`) REFERENCES `xy_usuarios` (`id_usu`);

--
-- Limitadores para a tabela `xy_posts`
--
ALTER TABLE `xy_posts`
  ADD CONSTRAINT `fkey_mjmxizhsvb` FOREIGN KEY (`id_topico`) REFERENCES `xy_topicos` (`id_topico`),
  ADD CONSTRAINT `fkey_prvdcadzzw` FOREIGN KEY (`id_usu`) REFERENCES `xy_usuarios` (`id_usu`),
  ADD CONSTRAINT `fkey_wntbmukoqj` FOREIGN KEY (`id_topico2`) REFERENCES `xy_topicos` (`id_topico`);
--
-- Database: `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(11) NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT '0',
  `x` float UNSIGNED NOT NULL DEFAULT '0',
  `y` float UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin,
  `data_sql` longtext COLLATE utf8_bin,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Estrutura da tabela `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Indexes for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Indexes for table `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Indexes for table `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Indexes for table `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Indexes for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Indexes for table `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Indexes for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Indexes for table `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Indexes for table `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Indexes for table `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Indexes for table `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Indexes for table `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Indexes for table `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Database: `portfolio`
--
CREATE DATABASE IF NOT EXISTS `portfolio` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `portfolio`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `assuntos`
--

CREATE TABLE `assuntos` (
  `cod_assunto` int(11) NOT NULL,
  `assunto` varchar(50) NOT NULL,
  `ext` varchar(5) NOT NULL DEFAULT '.jpg'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `cod_contato` int(11) NOT NULL,
  `nome` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `mensagem` varchar(150) NOT NULL,
  `ip` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `discussao`
--

CREATE TABLE `discussao` (
  `cod_usuario` int(11) NOT NULL,
  `cod_opiniao` int(11) NOT NULL,
  `cod_assunto` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `opinioes`
--

CREATE TABLE `opinioes` (
  `cod_opiniao` int(11) NOT NULL,
  `opiniao` varchar(50) DEFAULT NULL,
  `dt_inclusao` date DEFAULT NULL,
  `dt_alteracao` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `cod_usuario` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `login` varchar(30) NOT NULL,
  `senha` varchar(60) NOT NULL,
  `tipo` tinyint(1) NOT NULL DEFAULT '0',
  `dt_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assuntos`
--
ALTER TABLE `assuntos`
  ADD PRIMARY KEY (`cod_assunto`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`cod_contato`);

--
-- Indexes for table `discussao`
--
ALTER TABLE `discussao`
  ADD PRIMARY KEY (`cod_usuario`,`cod_assunto`,`cod_opiniao`),
  ADD KEY `fkey_jntxvcssih` (`cod_opiniao`),
  ADD KEY `fkey_gtpaashqog` (`cod_assunto`),
  ADD KEY `fkey_index_1` (`cod_usuario`);

--
-- Indexes for table `opinioes`
--
ALTER TABLE `opinioes`
  ADD PRIMARY KEY (`cod_opiniao`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`cod_usuario`),
  ADD UNIQUE KEY `new_index_1` (`login`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assuntos`
--
ALTER TABLE `assuntos`
  MODIFY `cod_assunto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `cod_contato` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `opinioes`
--
ALTER TABLE `opinioes`
  MODIFY `cod_opiniao` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `cod_usuario` int(11) NOT NULL AUTO_INCREMENT;
--
-- Database: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;
--
-- Database: `viaderegra`
--
CREATE DATABASE IF NOT EXISTS `viaderegra` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `viaderegra`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--
-- Error reading structure for table viaderegra.migrations: #1932 - Table 'viaderegra.migrations' doesn't exist in engine

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--
-- Error reading structure for table viaderegra.password_resets: #1932 - Table 'viaderegra.password_resets' doesn't exist in engine

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--
-- Error reading structure for table viaderegra.users: #1932 - Table 'viaderegra.users' doesn't exist in engine
--
-- Database: `wordpress`
--
CREATE DATABASE IF NOT EXISTS `wordpress` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `wordpress`;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
