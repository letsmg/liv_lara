<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Auth::routes();


Route::get('/', 'HomeController@index')->name('home.index_home');

Route::resource('/autores', 'AutoresController');
//aqui abaixo serve para o laravel montar o model de forma correta pois ele singulariza
//o nome do controller de maneira errada em outras linguas
Route::resource('autores', 'AutoresController')->parameters([
    'autores' => 'autor'
]);
Route::resource('editoras', 'EditorasController');
Route::resource('livros', 'LivrosController');



    // ->names(['create' => 'autores.cadastro'], //formulario de cadastro
    //         ['show' => 'autores.lista'], //lista todos
    //         ['destroy' => 'autores.exclui'], //exclui
    //         ['store' => 'autores.salva'], //salva o cadastro
    //         ['update' => 'autores.edita'], //salva a edicao
    //         ['edit' => 'autores.edicao']); //formulario de edicao
Route::resource('contato', 'ContatoController');

// o resources gera isso:
// GET/contacts, mapped to the index() method,
// GET /contacts/create, mapped to the create() method,
// POST /contacts, mapped to the store() method,
// GET /contacts/{contact}, mapped to the show() method,
// GET /contacts/{contact}/edit, mapped to the edit() method,
// PUT/PATCH /contacts/{contact}, mapped to the update() method,
// DELETE /contacts/{contact}, mapped to the destroy() method.