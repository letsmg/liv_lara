﻿INSTRUÇÕES

Neste projeto foi utlilizado o framework Laravel.

O link para download é:

https://gitlab.com/letsmg/liv_lara 
(precisa executar "composer install" no terminal apenas na primeira vez, para se manter
com as últimas alterações, usar o comando "git pull".)
**Favor dar preferência a essa opção**

ou 

https://www.manualdoesperto.com.br/liv_lara.zip 
(esse já está com todas dependências instaladas, mas atualizo com menos frequência)

Em ambos os casos, para usá-lo precisa, criar um banco de dados com nome "liv_lara"
ou qualquer se desejar outro nome, deve-se alterar o nome do banco no arquivo
".env"

Em seguida, duas formas de criar o banco:

1- através do terminal de comando, deve-se entrar na pasta do projeto e 
digitar o comando "php artisan make:migrate", o qual criará as tabelas de acordo 
com os arquivos de migração que criei no Laravel. É um forma prática de sempre ter uma versão da "ESTRUTURA" do banco de dados junto da aplicação.
**Favor dar preferência a essa opção**

ou

2- Importar o  "banco.sql" que está na raiz do projeto

Para acessar o projeto, abrir navegador de acordo com a configuração de seu servidor local, normalmente:
localhost/liv_lara/public
ou
127.0.0.1/liv_lara/public
ou se já estiver configurado para projetos laravel
127.0.0.1/liv_lara/

----------------------------------xxxxxxx-----------------------------------------

Todos controllers, models em migrations, foram criadas a partir de comandos do artisan.
Ferramenta que facilita o desenvolvimento.

Para exibição de imagens gravadas na pasta storage, foi usado o comando:
"php artisan storage:link"
que cria um atalho para as imagens no diretório público do framework.

Foi utilizado também o sistema Blade, que facitilita a reutilização de código, principalmente no que diz respeito a layout.

Já cheguei a utilizar em outro projeto o Laravel Tinker, Seeders e factories, porém 
tenho pouca lembrança do funcionamento. Se. que pode se adicionar dados ao banco para testes de forma rápida com esses e o tinker permite interação com o banco de dados pelo 
terminal de comando.

Utilizei nesse projeto, HTML5, CSS3 + Bootstrap 4 + SASS, JS, Composer, NPM/Yarn, Laravel Mix(Webpack), versionamento Git, Font Awesome, Xampp, DBNinja e PHPMyAdmin, MariaDB (MySql)entre outros.

Tenho minha biblioteca pessoal de arquivos CSS que uso em todos projetos para não precisar 
refazer sempre configurações que não tem no Bootstrap 4.

O framework Laravel também vem por padrão protegido contra diversos tipos de ataque, CSRF, 
XSS e Injeção de SQL;

Tenho um site feito em Laravel publicado, mas tem apenas um form de contato e não tenho 
mais acesso ao código fonte. Pertence a um dos sócios da empresa onde trabalhei:
www.autoplacasantarita.com.br

Outros sites feitos em Codeigniter publicados são:
www.viaderegra.com (pessoal)
www.manualdoesperto.com (pessoal)
www.pangareguaxupe.com.br
www.ferreiraesouzainformatica.com.br/blog