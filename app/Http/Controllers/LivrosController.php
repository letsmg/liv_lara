<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Livro;
use App\Autor;
use App\Editora;
use Illuminate\Support\Facades\Storage;
use Auth;



class LivrosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $livros = Livro::all();
        $livros = DB::table('liv_livros as l')
                     ->join('liv_autores as a', 'l.id_autor','a.id_autor')
                     ->join('liv_editoras as e','e.id_editora', 'l.id_editora')
                     ->join('users as u', 'l.id_user','u.id')
                     ->orderBy('id_livro','desc')                     
                     ->get();
        
        return view('livros.index_livro', compact('livros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $livros = Livro::all();
        $editoras = Editora::all();
        $autores = Autor::all();
        return view('livros.cadastro_livro',compact('livros','editoras','autores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titulo'=>'required',
            'capa'=>'required',
            'valor'=>'required',
            'autor'=>'required',
            'editora'=>'required',
        ]);


        $livros = new livro([
            'titulo' => $request->get('titulo'),
            'capa' => $request->get('capa'),
            'valor' => $request->get('valor'),
            'id_autor' => $request->get('autor'),
            'id_editora' => $request->get('editora'),
            'id_user' => Auth::user()->id
        ]);
        //SALVO SEM CADASTRAS NOMES DAS IMAGENS PARA PODER ADICIONAR SUAS ID´S AO NOME.
        //poderia até gerar um nome randomico, mas acredito que assim fica mais fácil para
        //manusear as imagens

        if($livros->save()){
            $id = $livros->id_livro;//id gerado pelo ultimo save
            $this->envia_arqs($request,$id);
            return redirect('/livros')->with('success', 'Cadastro salvo!');
        }
    }




    public function envia_arqs($request,$id)
    {
        //dd($request);
        //INICIO UPDATE NOME DAS IMAGENS NO BANCO
        for ($i=1; $i < 4; $i++) { //laço para atualizar nome imagens no banco, caso exista
            if($request->hasFile('img'.$i) && $request->file('img'.$i)->isValid()){ // Se informou o arquivo, retorna um boolean
                //$img1 = $request->img1->getClientOriginalName();
                $img['img'.$i] = $id.'_'.$i.'.'.$request->file('img'.$i)->getClientOriginalExtension();
            }else{
                $img['img'.$i] = "";
            }
        }

        $este_livro = livro::find($id);
        $este_livro->f1 = $img['img1'];
        $este_livro->f2 = $img['img2'];
        $este_livro->f3 = $img['img3'];
        $enviou = $este_livro->save();
        //FINAL UPDATE NOME DAS IMAGENS NO BANCO

        for ($i=1; $i < 4; $i++) {
            $arquivo_da_vez = 'img'.$i;

            if($request->hasFile('img'.$i) && $request->file('img'.$i)->isValid()){
                $file = $request->file($arquivo_da_vez);
                $extensao = $request->file($arquivo_da_vez)->getClientOriginalExtension();

                $upload = $request->file($arquivo_da_vez)->storeAs('livros/'.$id, $id.'_'.$i.'.'.$extensao);
            }
        }
    }





    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$livro = livro::find($id);
        $livro = DB::table('liv_livros as l')
                       ->join('liv_autores as a', 'l.id_autor','a.id_autor')
                       ->join('liv_editoras as e', 'l.id_editora', 'e.id_editora')
                       //->join('users as u', 'l.id_user','u.id')
                       ->where('id_livro',$id)
                       ->get();

        $editoras = Editora::all();
        $autores = Autor::all();
        return view('livros.edit_livro', compact('livro','editoras','autores'));
    }





    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'titulo'=> 'required',
            'capa'=> 'required',
            'valor'=> 'required',
            'editora'=>'required',
            'autor'=> 'required',
        ]);

        $livro = livro::find($id);
        $livro->titulo = $request->get('titulo');
        $livro->capa = $request->get('capa');
        $livro->valor = $request->get('valor');
        $livro->id_editora = $request->get('editora');
        $livro->id_autor = $request->get('autor');
        $livro->id_user = Auth::user()->id;

        if($livro->save()){
            $this->envia_arqs($request,$id);
            return redirect('/livros')->with('success', 'Livro atualizado com sucesso!');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $livro = livro::find($id);
        $livro->delete();

        return redirect('/livros')->with('success', 'livro '.$livro->livro.' apagado com sucesso!');
    }
}
