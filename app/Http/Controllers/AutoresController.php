<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Autor;
use Auth;
use DB;


class AutoresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$autores = Autor::all();
        $autores = DB::table('liv_autores as a')
                       ->join('users as u', 'a.id_user','u.id')                       
                       ->get();   
        
        return view('autores.index_autor', compact('autores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('autores.cadastro_autor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'autor'=>'required'
        ]);

        $autores = new Autor([
            'autor' => $request->get('autor'),
            'id_user' => Auth::user()->id
        ]);
        $autores->save();
        return redirect('/autores')->with('success', 'Cadastro salvo!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $autores = Autor::find($id);
        // $autores = DB::table('liv_autores as a')
        //                ->join('users as u', 'a.id_user','u.id')                       
        //                ->get();        
        
        return view('autores.edit_autor', compact('autores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'autor'=>'required'
        ]);

        $autor = Autor::find($id);
        $autor->autor = $request->get('autor');
        $autor->id_user = Auth::user()->id;
        $autor->save();

        return redirect('/autores')->with('success', 'Autor atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $autor = Autor::find($id);
        $autor->delete();

        return redirect('/autores')->with('success', 'Autor '.$autor->autor.' apagado com sucesso!');
    }
}
