<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Livro;
//use Illuminate\Pagination\Paginator; //nao precisa informar total de registros

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        //$livros = Livro::all();
        $paginacao = $request->n_por_pagina;

        if($paginacao !== null){
            $livros = Livro::simplePaginate($paginacao);
            //dd($livros);
            return view('home.index_home', compact('livros'));
        }else{
            $livros = Livro::simplePaginate(5);
            //dd($livros);
            return view('home.index_home', compact('livros'));
        }        
    }
}
