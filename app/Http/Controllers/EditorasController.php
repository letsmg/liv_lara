<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Editora;
use Auth;
use DB;

class EditorasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$editoras = Editora::all();
        $editoras = DB::table('liv_editoras as e')
                       ->join('users as u', 'e.id_user','u.id')                       
                       ->get();
                       
        return view('editoras.index_editora', compact('editoras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('editoras.cadastro_editora');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'editora'=>'required'
        ]);

        $editora = new editora([
            'editora' => $request->get('editora'),
            'id_user' => Auth::user()->id
        ]);
        $editora->save();
        return redirect('/editoras')->with('success', 'Cadastro salvo!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editoras = editora::find($id);
        
        return view('editoras.edit_editora', compact('editoras'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'editora'=>'required'
        ]);

        $editora = editora::find($id);
        $editora->editora = $request->get('editora');
        $editora->id_user = Auth::user()->id;
        
        $editora->save();

        return redirect('/editoras')->with('success', 'Editora atualizada com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $editora = editora::find($id);
        $editora->delete();

        return redirect('/editoras')->with('success', 'editora '.$editora->editora.' apagado com sucesso!');
    }
}
