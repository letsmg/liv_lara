<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {                
        Schema::defaultStringLength(191);

        //o artisan para de funcionar se mudar pra portugues
        //lá também diz que funciona com create e edit.. nao com os outros
        // Route::resourceVerbs([
        //     //'store' => 'salvar',
        //     //'show' => 'lista',            
        //     //'update' => 'edita',
        //     //'destroy' => 'exclui',
        //     'create' => 'cadastro',
        //     'edit' => 'detalhes'            
        // ]);
    }
}
