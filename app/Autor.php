<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    	/*
        o laravel sempre procura por padrao nome de
        tabela no banco no plural, mesmo que tenha criado
        no migration no singular, por isso tem de usar
        essa linha abaixo    
        */
        protected $table = 'liv_autores';
        
        const CREATED_AT = 'dt_cadastro';
        const UPDATED_AT = 'dt_alteracao';
    
        /*
        o laravel trabalha com todas colunas primarias sendo id,
        se quiser trocar, tem de usar a variável abaixo
        */
        protected $primaryKey = 'id_autor';
    
        protected $fillable = [
            'autor','id_user'
        ];
        
}
