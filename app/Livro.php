<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Livro extends Model
{
    	/*
        o laravel sempre procura por padrao nome de
        tabela no banco no plural, mesmo que tenha criado
        no migration no singular, por isso tem de usar
        essa linha abaixo    
        */
        protected $table = 'liv_livros';
        
        /*essas linhas abaixo são para alterar o nome padrão das colunas*/
        const CREATED_AT = 'dt_cadastro';
        const UPDATED_AT = 'dt_alteracao';
    
        /*
        o laravel trabalha com todas colunas primarias sendo id,
        se quiser trocar, tem de usar a variável abaixo
        */
        protected $primaryKey = 'id_livro';
    
        protected $fillable = [
            'titulo','capa','valor','id_autor','id_editora','id_user','f1','f2','f3'
        ];
        /*não são retornados em arrays*/
        
}
