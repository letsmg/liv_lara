const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    //.copy('node_modules/bootstrap/dist/css/bootstrap.min.css', 'css/bootstrap.min.css')
    //.copy('node_modules/bootstrap/dist/js/bootstrap.min.js', 'js/bootstrap.min.js')
    .copy('node_modules/font-awesome/css/font-awesome.min.css', 'public/css/font-awesome.min.css')
    .copy('node_modules/font-awesome/fonts/', 'public/fonts/')
    .copy('node_modules/jquery/dist/jquery.min.js', 'public/js/')
    //.copy('resources/vanillajs-scrollspy/dist/vanillajs-scrollspy.min.js', 'public/js/')
    //.copy('node_modules/holderjs/holder.min.js', 'public/js/')
    .sass('scss/geral/diversos.scss', 'public/css/diversos.css')
    .sass('scss/geral/margens.scss', 'public/css/margens.css')
    .sass('scss/geral/mediaqueries.scss', 'public/css/mediaqueries.css')
    .sass('scss/geral/backgrounds.scss', 'public/css/backgrounds.css')
    .sass('scss/geral/cores.scss', 'public/css/cores.css')
    .sass('scss/geral/fontes.scss', 'public/css/fontes.css')
	/* para juntar todos css num arquivo só, fica mais difícil de alguém roubar o código
		mas dificulta um pouco mais para identificar erros
	 .styles([
        'node_modules/bootstrap/dist/css/bootstrap.min.css', //une os arquivos css em um só
        'node_modules/font-awesome/css/font-awesome.min.css',//une os arquivos css em um só
        'scss/geral/css/diversos.css', //une os arquivos css em um só
        'scss/geral/css/margens.css', //une os arquivos css em um só
        'scss/geral/css/mediaqueries.css', //une os arquivos css em um só
		'scss/geral/css/backgrounds.css', //une os arquivos css em um só
		'scss/geral/css/cores.css', //une os arquivos css em um só
        'scss/geral/css/fontes.css', //une os arquivos css em um só        
    ],'css/compilado.css');*/
    ;