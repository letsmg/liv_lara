@extends('layouts.app')

@section('content')

    <div class="row bg-white sombra p-5">

        <div class="col-12 text-center">
            <a href="{{ route('home') }}" class='btn btn-primary'>
                <span class="fa fa-home"></span>
                Home
            </a>
            <a href="{{ route('autores.create') }}" class='btn btn-warning'>
                <span class="fa fa-plus"></span>
                Cadastro de Autores
            </a>
        </div>

        <div class="col-6 offset-3 text-center mt-3">
            @if(session()->get('success'))
                <div class="alert alert-success">
                {{ session()->get('success') }}
                </div>
            @endif


            @if ($errors->any())
                <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                </div><br />
            @endif
        </div>

        <div class="col-sm-12">
            <h1 class="display-3">Autores</h1>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <td>ID</td>
                        <td>Nome do Autor</td>
                        <td>Última Alteração</td>
                        <td colspan = 2>Ações</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($autores as $autor)
                        <tr>
                            <td>{{$autor->id_autor}}</td>
                            <td>{{$autor->autor}} </td>
                            <td>{{$autor->name}} </td>
                            <td>
                                <a href="{{ route('autores.edit',$autor->id_autor)}}" class="btn btn-success">Editar</a>
                            </td>
                            <td>
                                <form action="{{ route('autores.destroy', $autor->id_autor)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" onclick="return confirm('Tem certeza');" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection