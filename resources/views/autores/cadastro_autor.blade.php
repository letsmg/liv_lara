@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2 bg-white sombra p-5">
    <h1 class="display-5"><i class="fa fa-superpowers"></i> Cadastro de Autor</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('autores.store') }}">
          @csrf
          <div class="form-group">    
              <label for="autor">Autor</label>
              <input type="text" class="form-control" name="autor"/>
          </div>

                                   
          <button type="submit" class="btn btn-success">
            <i class="fa fa-save"></i>
            Salvar Autor
          </button>
      </form>
  </div>
</div>
</div>
@endsection