@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2 bg-white sombra p-5">
        <h1 class="display-5">Edição de livro</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('livros.update', $livro[0]->id_livro) }}" enctype="multipart/form-data">
            @method('PATCH') 
            @csrf

            ID {{ $livro[0]->id_livro }}

            <div class="form-group">                  
                <input type="text" class="form-control" name="titulo" value="{{$livro[0]->titulo}}"/>
                <div class='text-muted small'>Título</div>
            </div>
    
            <div class='form-group'>
                <select id='autor' name='autor' class="custom-select" >
                <option selected value="{{$livro[0]->id_autor}}">{{$livro[0]->autor}}</option>                            
                        <?php                        
                            foreach ($autores as $autor) {
                        ?>
                                <option value="<?= $autor->id_autor ?>" >
                                    <?= $autor->autor ?>
                                </option>
                        <?php
                            }
                        ?>
                    </div>
                </select>
                <div class='text-muted small'>Autor</div>
            </div>
    
            <div class='form-group'>
                <select id='editora' name='editora' class="custom-select" >
                        <option selected value="{{$livro[0]->id_editora}}">{{$livro[0]->editora}}</option>                            
                        <?php
                            foreach ($editoras as $editora) {
                        ?>
                                <option value="<?= $editora->id_editora ?>" >
                                    <?= $editora->editora ?>
                                </option>
                        <?php
                            }
                        ?>
                    </div>
                </select>
                <div class='text-muted small'>Editora</div>
            </div>
    
            <div class='form-group'>
                <label for='capa'></label>
                <input type='text' id='capa' name='capa' class='form-control' value="{{$livro[0]->capa}}"
                    required maxlength='30' >
                <div class='text-muted small'>Capa</div>
            </div>
    
            <div class='form-group'>
                <label for='valor'></label>
                <input type='text' id='valor' name='valor' class='form-control' value="{{$livro[0]->valor}}"
                    required maxlength='30' >
                <div class='text-muted small'>Valor</div>
            </div>

            <div class="form-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="img1" name="img1">
                    <label class="custom-file-label" for="img1">Procurar arquivo</label>
                </div>
            </div>

            <div class="form-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="img2" name="img2">
                    <label class="custom-file-label" for="img2">Procurar arquivo</label>
                </div>
            </div>

            <div class="form-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="img3" name="img3">
                    <label class="custom-file-label" for="img3">Procurar arquivo</label>
                </div>
            </div>  

            <div class="row">

                <div class="col-sm-4">
                    <img src="{{ url('storage/livros/'.$livro[0]->id_livro.'/'.$livro[0]->f1) }}" alt=""
                      title='' class='img-fluid rounded'>
                      <div class='text-muted small'>Imagem 1</div>
                </div>

                <div class="col-sm-4">
                    <img src="{{ url('storage/livros/'.$livro[0]->id_livro.'/'.$livro[0]->f2) }}" alt=""
                        title='' class='img-fluid rounded'>
                        <div class='text-muted small'>Imagem 2</div>
                </div>

                <div class="col-sm-4">
                    <img src="{{ url('storage/livros/'.$livro[0]->id_livro.'/'.$livro[0]->f3) }}" alt=""
                        title='' class='img-fluid rounded'>
                        <div class='text-muted small'>Imagem 3</div>
                </div>
            </div>

            <button type="submit" class="btn btn-success">
                <i class="fa fa-save"></i>
                Atualiza
            </button>
            <a href="{{ route('livros.index') }} " class='btn btn-secondary'>
                <span class="fa fa-arrow-circle-left"></span>
                Voltar
            </a>
            
        </form>
    </div>
</div>
@endsection