@extends('layouts.app')

@section('content')

    <div class="row bg-white sombra p-5">
        <div class="col-12 text-center">
            <a href="{{ route('home.index_home') }}" class='btn btn-primary'>
                <span class="fa fa-home"></span>
                Home
            </a>
            <a href="{{ route('livros.create') }}" class='btn btn-warning'>
                <span class="fa fa-plus"></span>
                Cadastro de Livros
            </a>
        </div>

        <div class="col-6 offset-3 text-center mt-3">
            @if(session()->get('success'))
                <div class="alert alert-success">
                {{ session()->get('success') }}
                </div>
            @endif


            @if ($errors->any())
                <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                </div><br />
            @endif
        </div>

        <div class="col-sm-12">
            <h1 class="display-3">Livros</h1> <p class="text-muted">Exibição mais recente/primeiro</p>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <td>ID</td>
                        <td>Nome do livro</td>
                        <td>Capa</td>
                        <td>Valor</td>
                        <td>Editora</td>
                        <td>Autor</td>
                        <td>Última Alteração</td>
                        <td colspan = 2>Ações</td>
                        </tr>
                    </thead>
                    <tbody>                        
                        @foreach($livros as $livro)
                        <tr>
                            <td>{{$livro->id_livro}}</td>
                            <td>{{$livro->titulo}} </td>
                            <td>{{$livro->autor}}</td>
                            <td>{{$livro->editora}}</td>
                            <td>{{$livro->capa}}</td>
                            <td>{{$livro->valor}}</td>
                            <td>{{$livro->name}} </td>
                            <td>
                                <a href="{{ route('livros.edit',$livro->id_livro)}}" class="btn btn-success">Editar</a>
                            </td>
                            <td>
                                <form action="{{ route('livros.destroy', $livro->id_livro)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" onclick="return confirm('Tem certeza');" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection