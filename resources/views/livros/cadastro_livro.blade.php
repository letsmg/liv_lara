@extends('layouts.app')

@section('content')


<div class="row">
 <div class="col-sm-8 offset-sm-2  bg-white sombra p-5">
    <h1 class="display-5"><i class="fa fa-book"></i> Cadastro de Livro</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('livros.store') }}" enctype="multipart/form-data">
          @csrf

          <div class="form-group">                  
              <input type="text" class="form-control" name="titulo" required/>
              <div class='text-muted small'>Título</div>
          </div>

          <div class='form-group'>
              <select id='autor' name='autor' class="custom-select" required>
                  <option selected value="">Escolha um autor...</option>                            
                      <?php                        
                          foreach ($autores as $autor) {
                      ?>
                              <option value="<?= $autor->id_autor ?>" >
                                  <?= $autor->autor ?>
                              </option>
                      <?php
                          }
                      ?>
                  </div>
              </select>
              <div class='text-muted small'>Autor</div>
          </div>

          <div class='form-group'>
              <select id='editora' name='editora' class="custom-select" required>
                  <option selected value="">Escolha uma editora...</option>                            
                      <?php
                          foreach ($editoras as $editora) {
                      ?>
                              <option value="<?= $editora->id_editora ?>" >
                                  <?= $editora->editora ?>
                              </option>
                      <?php
                          }
                      ?>
                  </div>
              </select>
              <div class='text-muted small'>Editora</div>
          </div>

          <div class='form-group'>
            <label for='capa'></label>
            <input type='text' id='capa' name='capa' class='form-control'
                 required maxlength='30' >
            <div class='text-muted small'>capa</div>
          </div>

          <div class='form-group'>
            <label for='valor'></label>
            <input type='text' id='valor' name='valor' class='form-control'
                 required maxlength='30' >
            <div class='text-muted small'>valor</div>
          </div>

          <div class="form-group">
              <div class="custom-file">
                  <input type="file" class="custom-file-input" id="img1" name="img1">
                  <label class="custom-file-label" for="img1">Procurar arquivo</label>
              </div>
          </div>

          <div class="form-group">
              <div class="custom-file">
                  <input type="file" class="custom-file-input" id="img2" name="img2">
                  <label class="custom-file-label" for="img2">Procurar arquivo</label>
              </div>
          </div>

          <div class="form-group">
              <div class="custom-file">
                  <input type="file" class="custom-file-input" id="img3" name="img3">
                  <label class="custom-file-label" for="img3">Procurar arquivo</label>
              </div>
          </div>
                                   
          <button type="submit" class="btn btn-success">
            <i class="fa fa-save"></i>
            Salvar Livro
          </button>
      </form>
  </div>
</div>
</div>



<?php
    if($_SERVER['SERVER_NAME'] == 'localhost'){
            //echo _SERVER['SERVER_NAME'];    
    ?>
    <!--  *********************************************** 
    script para testes 
    -->
    <script>
        $(document).ready(function(){
            $('#preenche').on('click',function(e){
                e.preventDefault();
                //var inputs = new Array();            
                $('input').each(function(){            
                    if($(this).attr('name') !== 'csrf_test_name' && $(this).attr('type') !== 'file' && $(this).attr('name') !== '_token'){
                    //inputs.push($(this).val());
                    min = Math.ceil(0);
                    max = Math.floor(99);
                    $("input[name="+$(this).attr('name')+"]").val($(this).attr('name')+'_'+Math.floor(Math.random() * (max - min)));
                    }
                });
                //$('input[type=text]').val($(this).attr('name')); //deixa todos iguais
                $('input[type=date]').val('1985-12-06');
                $('input[type=email]').val('teste@teste.com');
                $('input[type=number]').val(123);
                //$('option[value=MG]').attr('selected','selected');
                $("select").prop("selectedIndex", 2);
                $('input[type=checkbox]').attr('checked','checked');
                $('textarea').each(function(){
                   if($(this).attr('name') !== 'csrf_test_name' && $(this).attr('type') !== 'file'){
                   //inputs.push($(this).val());
                   $("textarea[name="+$(this).attr('name')+"]").val($(this).attr('name'));
                   }
               });
            });
        });
    </script>

    <div class='container bg-white text-center'>
        <a href="#" id="preenche" class="btn btn-success">
            <span class="fa fa-plus"></span>
            Preencher inputs
        </a>
    </div>
    <!--  *********************************************** 
        script para testes 
    -->
    <?php
    }
?>

@endsection