@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2 bg-white sombra p-5">
    <h1 class="display-5">Cadastro de Editora</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('editoras.store') }}">
          @csrf
          <div class="form-group">                  
              <input type="text" class="form-control" name="editora"/>
              <div class='text-muted small'>Nome</div>
          </div>

                                   
          <button type="submit" class="btn btn-success">
            <i class="fa fa-save"></i>
            Salvar Editora
          </button>
      </form>
  </div>
</div>
</div>
@endsection