@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2 bg-white sombra p-5">
        <h1 class="display-5">Edição de editora</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('editoras.update', $editoras->id_editora) }}">
            @method('PATCH') 
            @csrf
            <div class="form-group">

                <label for="editora">Nome do editora:</label>
                <input type="text" class="form-control" name="editora" value="{{ $editoras->editora }}" />
            </div>

            
            <button type="submit" class="btn btn-success">
                <i class="fa fa-save"></i>
                Atualiza
            </button>

            <a href="{{ route('editoras.index') }} " class='btn btn-secondary'>
                <span class="fa fa-arrow-circle-left"></span>
                Voltar
            </a>
        </form>
    </div>
</div>
@endsection