@extends('layouts.app')

@section('content')



    <div class="row bg-white sombra p-5">

        <div class="col-12 text-center">
            <a href="{{ route('home') }}" class='btn btn-primary'>
                <span class="fa fa-home"></span>
                Home
            </a>
            <a href="{{ route('editoras.create') }}" class='btn btn-warning'>
                <span class="fa fa-plus"></span>
                Cadastro de Editoras
            </a>
        </div>

        <div class="col-6 offset-3 text-center mt-3">
            @if(session()->get('success'))
                <div class="alert alert-success">
                {{ session()->get('success') }}
                </div>
            @endif


            @if ($errors->any())
                <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                </div><br />
            @endif
        </div>

        <div class="col-sm-12">
            <h1 class="display-3">Editoras</h1>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <td>ID</td>
                        <td>Nome do editora</td>
                        <td>Última Alteração</td>
                        <td colspan = 2>Ações</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($editoras as $editora)
                        <tr>
                            <td>{{$editora->id_editora}}</td>
                            <td>{{$editora->editora}}</td>
                            <td>{{$editora->name}} </td>
                            <td>
                                <a href="{{ route('editoras.edit',$editora->id_editora)}}" class="btn btn-success">Editar</a>
                            </td>
                            <td>
                                <form action="{{ route('editoras.destroy', $editora->id_editora)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" onclick="return confirm('Tem certeza');" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection