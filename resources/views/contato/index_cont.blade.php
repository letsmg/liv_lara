@extends('layouts.app')

@section('content')



<div class="row">

    <div>
      <a style="margin: 19px;" href="{{ route('contato.create')}}" class="btn btn-primary">New contact</a>
    </div>  

<div class="col-sm-12">
    <h1 class="display-3">contato</h1>    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Name</td>
          <td>Email</td>
          <td>Job Title</td>
          <td>City</td>
          <td>Country</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($contato as $cont)
        <tr>
            <td>{{$cont->id}}</td>
            <td>{{$cont->first_name}} {{$cont->last_name}}</td>
            <td>{{$cont->email}}</td>
            <td>{{$cont->job_title}}</td>
            <td>{{$cont->city}}</td>
            <td>{{$cont->country}}</td>
            <td>
                <a href="{{ route('contato.edit',$cont->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('contato.destroy', $cont->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>

<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

@endsection