<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    @include('layouts.cabecalho')

    <body>
        <div id="app">

            @include('layouts.menu')

            <main class="container py-50">
                @yield('content')
            </main>
        </div>
    </body>
</html>
