
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="pt-br">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="desenvolvimento de sites, criação de sites, sites Sul de Minas, Programador, preço de site, contratar desenvolvedor, Poços de Caldas, Alfenas, Varginha">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="Luiz Eduardo">

    <?php
        if(isset($detalhes->tag_title)){
    ?>
        <title><?= $detalhes->tag_title  ?> | Portfolio Luiz Eduardo - Via de Regra</title>
        <meta name="title" content ="<?= $detalhes->tag_title  ?> | Portfolio Luiz Eduardo - Via de Regra" >
        <?php /*<meta name="description" CONTENT="<?= substr($detalhes->secao,0,152) ?>">*/ ?>
        <meta name="description" CONTENT="<?= $detalhes->tag_description ?>">
        <meta property="og:title"         content="<?= $detalhes->tag_title ?> | Portfolio Luiz Eduardo - Via de Regra" />
        <?php /*<meta property="og:description"   content="<?= substr($detalhes->secao,0,152) ?>" />*/ ?>
        <meta property="og:description"   content="<?= $detalhes->tag_description ?>" />
    <?php
        }else{
    ?>
        <title>Portfolio Luiz Eduardo - Desenvolvedor</title>
        <meta name="title" content ="Portfolio Luiz Eduardo - Desenvolvimento de sites para Minas e Brasil" >
        <meta name="description" CONTENT="Portfolio Luiz Eduardo - Desenvolvimento de site responsivo, site bonito, com otimização de SEO e Marketing Digital por preço justo.">
        <meta property="og:title"         content="Portfolio Luiz Eduardo - Desenvolvimento de sites para Minas e Brasil" />
        <meta property="og:description"   content="Portfolio Luiz Eduardo - Desenvolvimento de site responsivo, site bonito, com otimização de SEO e Marketing Digital por preço justo." />
    <?php
        }    
        if(isset($_SESSION['codigo'])){ 
        //se estiver logado nao deve ser indexado nem seguido
    ?>
        <meta name="robots" content="noindex, nofollow">
    <?php
        }else{
    ?>
        <meta name="robots" content="index, follow">
    <?php
        }
    ?>

    <link rel="amphtml" href="https://www.viaderegra.com.br/">
    <link rel="dns-prefetch" href="https://www.viaderegra.com">
    <link rel="dns-prefetch" href="https://www.viaderegra.com.br">
    <link rel="dns-prefetch" href="https://www.manualdoesperto.com">
    <link rel="dns-prefetch" href="https://www.manualdoesperto.com.br">        
    <link rel='dns-prefetch' href='https://www.facebook.com/luizeduardots' />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/jquery.min.js') }}" ></script>    
    <script src="{{ asset('js/holder.min.js') }}" ></script>    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="{{ asset('css/margens.css') }}" rel="stylesheet">
    <link href="{{ asset('css/cores.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mediaqueries.css') }}" rel="stylesheet">
    <link href="{{ asset('css/backgrounds.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fontes.css') }}" rel="stylesheet">
    <link href="{{ asset('css/diversos.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
</head>