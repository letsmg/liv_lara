<script>
    $(document).ready(function(){
        $('li a').click(function (e) {
            //alert($(this).attr('href'));
            if($(this).attr('href')=='#livros'){
                e.preventDefault();

                $('html,body').animate({
                    scrollTop: $(this).offset().top +250
                }, 1000);
            }
        });
    });
</script>




    @unless (Auth::check()) {{-- so mostra se não estiver logado --}}
        <div class="parallax py-150" >
            <div class="col-6 offset-6 bg-translucido text-white negrito p-3 text-right rounded montserrat" >
                <h2>Livraria Laravel</h2>
                <h4>Venha mergulhar no mundo da leitura!</h4>
            </div>
        </div>
    @endunless

    <nav class="navbar navbar-expand-md navbar-dark bg-azul shadow-sm sticky-top" >
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ url('/storage/lara.png') }}" width="40" height="40" alt="">
                <i class="fa fa-home"></i>{{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        @if(\Request::is('/') || \Request::is('/#livros') )
                            <li class="nav-item">
                                <a class="nav-link active" href="#livros">Nossos Livros</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ url('/#livros') }}">Nossos Livros</a>
                            </li>
                        @endif
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ route('register') }}">{{ __('Cadastre-se') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle " href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-book"></i>Livros
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('livros.index') }}"><i class="fa fa-list"></i>Lista</a>
                            <a class="dropdown-item" href="<?= route('livros.create'); ?>"><i class="fa fa-user-plus"></i> Cadastro de Livros</a>

                            </div>
                        </li>

                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle " href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-superpowers"></i>Autores
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('autores.index') }}"><i class="fa fa-list"></i>Lista</a>
                            <a class="dropdown-item" href="<?= route('autores.create'); ?>"><i class="fa fa-user-plus"></i> Cadastro de Autores</a>

                            </div>
                        </li>

                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle " href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-users"></i>Editoras
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('editoras.index') }}"><i class="fa fa-list"></i>Lista</a>
                            <a class="dropdown-item" href="<?= route('editoras.create'); ?>"><i class="fa fa-user-plus"></i> Cadastro de Editoras</a>

                            </div>
                        </li>



                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>