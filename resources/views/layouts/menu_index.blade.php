<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>

    
<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-verde py-3 mb-5">
  
  <a class="navbar-brand" href="<?= base_url(); ?>">
    <img src="<?= base_url('img/icones/logo.jpg'); ?>" alt="Livraria Babilônia"
        title='Livraria Babilônia' class='rounded' width='50px'>
        Livraria Babilônia
  </a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto sem-sub-branco">

      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('livros/lista'); ?>"><i class="fa fa-book"></i>Livros</a>
      </li>
      
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('autores/lista'); ?>"><i class="fa fa-users"></i>Autores</a>
      </li>

      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('editoras/lista'); ?>"><i class="fa fa-list"></i>Editoras</a>
      </li>

            
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle " href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-user-plus"></i>Usuários
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?= base_url('login'); ?>">Login</a>
          <?php          
            if($_SERVER['HTTP_HOST'] == 'localhost'){            
          ?>
          <a class="dropdown-item" href="<?= base_url('usuarios/novo'); ?>">Cadastre-se</a>          
          <?php
            }
          ?>
        </div>
      </li>
      
    </ul>
    <!-- <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Procurar" aria-label="Procurar">
      <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Procurar</button>
    </form> -->
  </div>
</nav>
