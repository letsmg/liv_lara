{{-- inicio paginacao 1 --}}
{{-- {{ dd($livros) }} --}}
<div class="col-12">
    <div class="align-items-center">
    @if ($livros->hasPages())
        <ul class="pagination justify-content-center" role="navigation">
            {{-- Previous Page Link --}}
            @if ($livros->onFirstPage())
                <li class="page-item disabled" aria-disabled="true">
                    <span class="page-link">@lang('pagination.previous')</span>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="{{ $livros->previousPageUrl() }}" rel="prev">@lang('pagination.previous')</a>
                </li>
            @endif

            {{-- Next Page Link --}}
            @if ($livros->hasMorePages())
                <li class="page-item">
                    <a class="page-link" href="{{ $livros->nextPageUrl() }}" rel="next">@lang('pagination.next')</a>
                </li>
            @else
                <li class="page-item disabled" aria-disabled="true">
                    <span class="page-link">@lang('pagination.next')</span>
                </li>
            @endif
        </ul>
    @endif
    </div>
</div>
{{-- final paginacao 1 --}}