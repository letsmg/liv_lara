@extends('layouts.app')


@section('content')

    <div class="container">
        @if (Auth::check())
            <div class="row justify-content-center">
                <div class="alert alert-info">
                    Bem-vindo ao sistema {{Auth::user()->name}} !
                </div>
            </div>
        @else
            <div class="row">

                <div class="col-md-6">
                    <div class="embed-responsive embed-responsive-16by9 my-3">
                        <iframe class="embed-responsive-item" width="801" height="451" src="https://www.youtube.com/embed/8D7e98p34T4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>

                <div class="col-md-6 text-22">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Novidades</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#perfil" role="tab" aria-controls="profile" aria-selected="false">Livro do Mês</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contato" role="tab" aria-controls="contact" aria-selected="false">Mais pedidos</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            Agora você tem um desconto de 10% em qualquer livro do autor fulano de tal
                        </div>
                        <div class="tab-pane fade" id="perfil" role="tabpanel" aria-labelledby="profile-tab">
                            O livro desse mês é "o mistério de quem fui" do contemporâneo escritor e poeta
                            João Julios Josier.
                        </div>
                        <div class="tab-pane fade" id="contato" role="tabpanel" aria-labelledby="contact-tab">
                            Os mais pedidos desse mês foram:
                            1- era uma vez
                            2- mais do mesmo
                            3- sabe-se lá
                        </div>
                    </div>
                </div>

            </div>
        @endif

        <script>
            $(document).ready(function(){
                $('#n_por_pagina').on('change',function(){
                    $('#paginacao').submit();
                })
            });
        </script>


        <!-- inicio seção central -->
        <section id='central' class='row justify-content-center'>

            <div id="livros" class="container my-5">
                <div class="row">
                    <div class="col-sm-6">
                        <h3>Nossos livros</h3>
                    </div>
                    <div class="col-sm-6">
                        {{-- <form id='paginacao' class='form-inline' action="{{ route('home.index_home') }}">
                            @csrf

                            <div class='form-group'>
                                <select id='n_por_pagina' name='n_por_pagina' class="custom-select" >
                                    <option selected value="">N. por página</option>
                                    <option value="2">2</option>
                                    <option value="4">4</option>
                                    <option value="8">8</option>                                    
                                </select>
                            </div>
                        </form> --}}
                    </div>
                </div>

                <hr>
            </div>

            @include('home.paginacao_home')
            
            {{-- esse comando abaixo já faz um foreach e joga na view chamada 
                mas ainda não achei como fazer interações com o loop 
                "a variável usada não é $livros --}}
            {{-- @each('home.livros_home', $livros,'livro') --}}
            @include('home.livros_home')
                
            @include('home.paginacao_home')

        </section>
        <!-- final seção central carousel -->


    </div>

@endsection

