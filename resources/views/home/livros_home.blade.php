@php
    $i = 1;
@endphp

@foreach ($livros as $livro)
    {{-- inicio exibicao de livros --}}
    <div class="col-sm-6 col-md-4 col-lg-3 sem-sub-grupo my-3">
        
        {{-- {{ dd($livros) }} --}}
        <div class="card">
            <?php
                if(empty($livro->f1) && empty($livro->f2) && empty($livro->f3)){
            ?>
                    <div class="">
                        <img class="card-img-top" src="<?= url('/storage/sem_foto.png') ?>" alt="sem foto">
                    </div>
            <?php
                }else{
            ?>

                    <div id="carousel{{$i}}" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel{{$i}}" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel{{$i}}" data-slide-to="1"></li>
                            <li data-target="#carousel{{$i}}" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">

                        <?php
                            if(!empty($livro->f1)){
                        ?>
                                <div class="carousel-item active">
                                    <img class="img-fluid" data-src="holder.js/100x100" src="<?= url('storage/livros/'.$livro->id_livro.'/'.$livro->f1) ?>" alt="<?= $livro->f1 ?>">
                                </div>


                        {{-- parte 2 --}}
                        <?php
                            }
                            if(!empty($livro->f2)){ //se nao tiver img2 já nao exibe nada
                                if(empty($livro->f1)){ //se não houver foto 1 tem de usar a classe active aqui
                            ?>
                                    <div class="carousel-item active">
                        <?php
                                }else{
                        ?>
                                    <div class="carousel-item">
                        <?php
                                }
                        ?>
                                        <img class="img-fluid" data-src="holder.js/100x100" src="<?= url('storage/livros/'.$livro->id_livro.'/'.$livro->f2) ?>" alt="<?= $livro->f2 ?>">
                                    </div>


                        {{-- parte 3 --}}
                        <?php
                            }
                            if(!empty($livro->f3)){
                                if(empty($livro->f1) && empty($livro->f2)){ //se não houver f1 e f2 tem de usar a classe active aqui
                        ?>
                                    <div class="carousel-item active">
                        <?php
                                }else{
                        ?>
                                    <div class="carousel-item">
                        <?php
                                }
                        ?>
                                        <img class="img-fluid" data-src="holder.js/100x100" src="<?= url('storage/livros/'.$livro->id_livro.'/'.$livro->f3) ?>" alt="<?= $livro->f3 ?>">
                                    </div>
                        <?php
                            }
                        ?>

                        </div>

                        <a class="carousel-control-prev" href="#carousel{{$i}}" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel{{$i}}" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        
                    </div>
            @php
                }
            @endphp


            <div class="card-body">
                <h5 class="card-title"><?= $livro->titulo ?></h5>
                <p class="card-text text-muted"><?= $livro->autor ?></p>
                <p class="card-text text-muted">Valor <?= $livro->valor ?></p>
                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal<?= $livro->id_livro ?>">
                    Detalhes
                </button>
            </div>
        </div>
    </div>
    {{-- final exibicao de livros --}}

    {{-- inicio modal --}}
    <div class="modal fade" id="modal<?= $livro->id_livro ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?= $livro->titulo ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img src="<?= url('storage/livros/'.$livro->id_livro.'/'.$livro->f1); ?>" alt="<?= $livro->f1 ?>"
                    title='<?= $livro->f1 ?>' class='img-fluid rounded'>

                <br>
                Autor: <?= $livro->autor ?>
                <br>
                Editora: <?= $livro->editora ?>
                <br>
                Capa: <?= $livro->capa ?>
                <br>
                Valor: <?= $livro->valor ?>
                <br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
            </div>
        </div>
    </div>

    @php
        $i++
    @endphp
    {{-- final modal --}}
@endforeach